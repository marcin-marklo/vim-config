source $VIMRUNTIME/vimrc_example.vim

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'tpope/vim-fugitive'
Plugin 'bling/vim-airline'
"Bundle 'noah/vim256-color'
Plugin 'derekwyatt/vim-scala'
Plugin 'Valloric/YouCompleteMe'
Plugin 'elzr/vim-json'
Plugin 'kien/ctrlp.vim'
Plugin 'pangloss/vim-javascript'
Plugin 'othree/javascript-libraries-syntax.vim'
Plugin 'noahfrederick/vim-hemisu'

call vundle#end()            " required
filetype plugin indent on    " required

set t_Co=256
set ts=4
set sw=4
set smarttab
set expandtab
set autoindent
set laststatus=2
set showcmd
set nobackup

set background=light
colo hemisu

" options for ctr-p
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
" end

" options for javascript-libraries-syntax
let g:used_javascript_libs = 'angularjs,angularui'
" end
